extends KinematicBody2D

export (int) var speed = 400
export (int) var teleport_speed = 5000
export (int) var GRAVITY = 1200
export (int) var jump_speed = -600
export (int) var jump_limit = 4
export (int) var jump_count = 0


const UP = Vector2(0,-1)

var velocity = Vector2()

func get_input():
	# press X to teleport (you can do it for an infinite number of times)
	# press Z to dash (increase speed while pressed)
	velocity.x = 0
	if is_on_floor():
		jump_count = 0
		$Sprite.frame = 0
	if is_on_floor() and Input.is_action_just_pressed('up'):
		velocity.y = jump_speed
		jump_count += 1
		$Sprite.frame = 1
	if Input.is_action_pressed('right'):
		velocity.x += speed
		$Sprite.flip_h = false
		if Input.is_action_just_pressed("teleport"):
			velocity.x += teleport_speed
		if Input.is_action_pressed("dash"):
			velocity.x += speed
	if Input.is_action_pressed('left'):
		velocity.x -= speed
		$Sprite.flip_h = true
		if Input.is_action_just_pressed("teleport"):
			velocity.x -= teleport_speed
		if Input.is_action_pressed("dash"):
			velocity.x -= speed
	if !is_on_floor() and Input.is_action_just_pressed('up') and jump_count < jump_limit:
		velocity.y = jump_speed
		jump_count += 1


func _physics_process(delta):
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
